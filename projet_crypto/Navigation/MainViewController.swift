//
//  MainViewController.swift
//
//  Created by Matthieu SAINT-MARTIN on 24/04/2018.
//  Copyright © 2018 Matthieu SAINT-MARTIN. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        let allCurrencies = CurrenciesViewController()
        let tabTwoBarItem1 = UITabBarItem(title: "", image: #imageLiteral(resourceName: "Icon 1"), selectedImage: #imageLiteral(resourceName: "Icon 1"))
        allCurrencies.tabBarItem = tabTwoBarItem1
        
        let wallet = WalletViewController()
        let tabTwoBarItem2 = UITabBarItem(title: "", image: #imageLiteral(resourceName: "Icon 2"), selectedImage: #imageLiteral(resourceName: "Icon 2"))
        wallet.tabBarItem = tabTwoBarItem2
        
        let thirdTab = ThirdTabViewController()
        let tabTwoBarItem3 = UITabBarItem(title: "", image: #imageLiteral(resourceName: "Icon 3"), selectedImage: #imageLiteral(resourceName: "Icon 3"))
        thirdTab.tabBarItem = tabTwoBarItem3
        
        self.viewControllers = [allCurrencies, wallet, thirdTab]
        
         self.tabBar.isTranslucent = true
        UITabBar.appearance().barTintColor = UIColor.black
        self.tabBar.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("select Index :\(self.selectedIndex)")
    }

}
