//
//  CurrencyTableViewCell.swift
//
//  Created by Matthieu SAINT-MARTIN on 16/01/2018.
//  Copyright © 2018 Matthieu SAINT-MARTIN. All rights reserved.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {

//    @IBOutlet weak var imageCrypto: UIImageView!
    @IBOutlet weak var nameCrypto: UILabel!
    @IBOutlet weak var tokenCypto: UILabel!
    @IBOutlet weak var valueCrypto: UILabel!
    @IBOutlet weak var percentageCrypto: UILabel!
    @IBOutlet weak var progressImageCrypto: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
