//
//  CurrencyDetailsViewController.swift
//
//  Created by Matthieu SAINT-MARTIN on 16/01/2018.
//  Copyright © 2018 Matthieu SAINT-MARTIN. All rights reserved.
//

import UIKit

class CurrencyDetailsViewController: UIViewController {

    @IBOutlet weak var currencyNameLabel: UILabel!
    
    var currency: Monney?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.currencyNameLabel.text = self.currency?.name
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
