//
//  CurrenciesViewController.swift
//
//  Created by Matthieu SAINT-MARTIN on 16/01/2018.
//  Copyright © 2018 Matthieu SAINT-MARTIN. All rights reserved.
//

import UIKit
import CryptoCurrencyKit

let url = "https://api.coinmarketcap.com/v1/ticker/?convert=EUR"

struct CryptoCurrency {
    var name: String
    var valueEuro: Double
    var token: String
    var percent_change: Double
}

class CurrenciesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    var currenciesTab: [CryptoCurrency] = []
    
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var orderValue: UIButton!
    @IBOutlet weak var imageOrder: UIImageView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var currencyTableView: UITableView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var okButton: UIButton!
    
    var currentColor = UIColor.white
    
    var cryptoArray: [Monney]?
    
    var indentifier = "CurrencyTableViewCell"
    
    let choices: [String] = ["Nom","Value"]
    
    var croissant = true
    
    @IBOutlet weak var pickerBottomConstrain: NSLayoutConstraint!
    
    @IBAction func togglePicker(_ sender: UIButton) {
        
        if(sender == okButton){
            
            switch choices[pickerView.selectedRow(inComponent: 0)]{
            case "Nom":
                self.orderValue.setTitle("Nom", for: .normal)
                if croissant{
                    self.cryptoArray?.sort(by: {$0.name < $1.name})
                    self.imageOrder.image = #imageLiteral(resourceName: "Order")
                }
                else{
                    self.cryptoArray?.sort(by: {$0.name > $1.name})
                    self.imageOrder.image = #imageLiteral(resourceName: "Order_Reverse")
                }
                self.croissant = !self.croissant
                self.currencyTableView.reloadData()
                break
            case "Value":
                self.orderValue.setTitle("Value", for: .normal)
                if croissant{
                    self.cryptoArray?.sort(by: {$0.valueDollars < $1.valueDollars})
                    self.imageOrder.image = #imageLiteral(resourceName: "Order")
                }
                else{
                    self.cryptoArray?.sort(by: {$0.valueDollars > $1.valueDollars})
                    self.imageOrder.image = #imageLiteral(resourceName: "Order_Reverse")
                }
                self.croissant = !self.croissant
                self.currencyTableView.reloadData()
                break
            default: break
                self.imageOrder.image = nil
            }
            
        }
        
        self.pickerBottomConstrain.constant = self.pickerBottomConstrain.constant == 0 ? (self.pickerView.frame.height+100) : 0
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        
        loadData()
        currencyTableView.register(UINib(nibName: self.indentifier, bundle: nil), forCellReuseIdentifier:self.indentifier)
        
        self.currencyTableView.delegate = self
        self.currencyTableView.dataSource = self
        
//        refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
//        currencyTableView.backgroundView = refreshControl
        
        
        self.searchTextField.addTarget(self, action: #selector(closeSearch), for: .editingDidEndOnExit)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func loadData(){
        self.cryptoArray = []
        CryptoCurrencyKit.fetchTickers { response in
            switch response {
            case .success(let data):
                for crypto in data{
                    if let valueDollars = crypto.priceUSD,
                        let percentChange = crypto.percentChange1h{
                        self.cryptoArray?.append(Monney(name: crypto.name, token: crypto.symbol, valueDollars: valueDollars, percentChange: percentChange))
                        
                    }
                    self.currencyTableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    @objc func endRefreshState(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        self.refreshControl.endRefreshing()
    }
    
    @objc func closeSearch(_ sender: Any) {
        self.searchTextField.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.cryptoArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.indentifier, for: indexPath) as! CurrencyTableViewCell
        
        let crypto = cryptoArray![indexPath.row]
        
            cell.nameCrypto.text = crypto.name
        
        if(crypto.percentChange > 0){
            cell.percentageCrypto.textColor = UIColor(red: 0, green: 255, blue: 0, alpha: 1)
            cell.progressImageCrypto.image = #imageLiteral(resourceName: "up")
        }
        else{
            cell.percentageCrypto.textColor = UIColor(red: 255, green: 0, blue: 0, alpha: 1)
            cell.progressImageCrypto.image = #imageLiteral(resourceName: "down")
        }
        cell.percentageCrypto.text = "\(crypto.percentChange)"
        cell.tokenCypto.text = "\(crypto.token)"
        cell.valueCrypto.text = "\(crypto.valueDollars)"
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let monney = self.cryptoArray![indexPath.row]
        let vc = CurrencyDetailsViewController()
        vc.currency = monney
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return choices.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return choices[row]
    }
}
