//
//  Monney.swift
//
//  Created by Matthieu SAINT-MARTIN on 24/04/2018.
//  Copyright © 2018 Matthieu SAINT-MARTIN. All rights reserved.
//

import Foundation


class Monney{
    
    var name: String
    var token: String
    var valueDollars: Double
    var percentChange: Double

    init(name: String,token: String, valueDollars: Double, percentChange: Double) {
        self.name = name
        self.token = token
        self.valueDollars = valueDollars
        self.percentChange = percentChange
    }

    var description: String{
        return "name : \(self.name)"
    }
    
}
